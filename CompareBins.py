#import argparse
from glob import glob
import ROOT
import os, sys
from array import *
from ROOT import *#TCanvas,TLegend,gROOT
import math
import time

ROOT.gROOT.LoadMacro("AtlasStyle.C")
ROOT.gROOT.LoadMacro("AtlasUtils.C")
SetAtlasStyle()


tmpVale=sys.argv
def Helper():
    print()
    print(" Usage: python PrintROC.py file1 file2 leg1 leg2 outfolder tagger")
    print()
    sys.exit(1)
if len(sys.argv) <7: Helper()

odir= sys.argv[5]
tag = sys.argv[6]
########################################################################################
#########################################################################################
gSystem.Exec("mkdir -p "+odir)

infile1=sys.argv[1]
infile2=sys.argv[2]

leg1=sys.argv[3]
leg2=sys.argv[4]

f1=TFile(infile1,"R")
f2=TFile(infile2,"R")

# here we create the axes that we draw our ROC curves on
#light=TH1F("b VS light","b VS light",100,0.2,1);
#light.SetTitle(";b-jet efficiency;light-jet rejection;")
#lightCurve=[]
#light.SetMinimum(1e0)
#light.SetMaximum(1e5)
#light.GetXaxis().SetRangeUser(0.2,1.0)


for flav in ["l","c"]:
	# now we create the ROC curves #ignore name light needs change
	light=TH1F("b VS "+flav,"b VS "+flav,100,0.2,1);
	light.SetTitle(";b-jet efficiency;"+flav+"-jet rejection;")
	lightCurve=[]
	light.SetMinimum(1e0)
	light.SetMaximum(1e5)
	light.GetXaxis().SetRangeUser(0.2,1.0)
	f1.cd()  # file within the file
	curve = f1.Get(tag+"---b"+flav)#"MV2c10---bl")
	curve.SetLineColor(1)
	curve.SetLineWidth(3)

	f2.cd()
	curve2 = f2.Get(tag+"---b"+flav)#MV2c10---bl")
	curve2.SetLineColor(2)
	curve2.SetLineStyle(2)
	curve2.SetLineWidth(3)

	# and legend
	legend=TLegend(0.55,0.7,0.92,0.90)
	legend.SetTextFont(42)
	legend.SetTextSize(0.042)
	legend.SetFillColor(0)
	legend.SetLineColor(0)
	legend.SetFillStyle(0)
	legend.SetBorderSize(0)
	legend.AddEntry(curve,leg1,"L")
	legend.AddEntry(curve2,leg2,"L")
	legend.Draw("SAME")

	# create ratio, plot with ROC on same canvas
	myCx=TCanvas( "bVS"+flav, "bVS"+flav,800,800);

	pad_1=TPad("pad_1", "up", 0., 0.35, 1., 1.);
	pad_1.SetBottomMargin(0);
	pad_1.Draw();   
	pad_1.SetGridy()
	pad_1.SetGridx()
	pad_1.SetLogy()
	pad_2=TPad("pad_2", "down", 0.0, 0.00, 1.0, 0.35);
	pad_2.SetTopMargin(0);
	pad_2.SetBottomMargin(0.28);
	pad_2.Draw();
	pad_2.SetGridy()
	pad_2.SetGridx()
	pad_1.cd()
	light.Draw()
	curve.Draw("C")
	curve2.Draw("C")
	legend.Draw("SAME")

	myCx.Update()

	pad_2.cd()

	# create axes for ratio
	ratio = light.Clone("ratio")
	#ratio.Sumw2()
	ratio.GetYaxis().SetTitle("Ratio: "+leg2+"/"+leg1)
	ratio.GetYaxis().SetTitleOffset(1.1)
	ratio.GetXaxis().SetLabelSize(0.10)
	ratio.GetXaxis().SetTitleSize(0.10)
	ratio.GetYaxis().SetTitleOffset(0.7)
	ratio.GetYaxis().SetLabelSize(0.09)
	ratio.GetYaxis().SetTitleSize(0.09)
	ratio.SetMaximum(1.3)
	ratio.SetMinimum(0.7)
	ratio.SetLineColor(kGray)
	ratio.Draw("HIST")

	# now we create the ratio curve

	ratC = curve.Clone("ratioC")
	countPoint = 0

	print(" looping over: " + str(curve.GetN()) + "  points")

	for mybin in xrange(0,curve.GetN()):  # really necessary to add 1?
		px1=Double(0)
		py1=Double(0)
		curve.GetPoint(mybin, px1, py1)
		px2=Double(0)
		py2=Double(0)
		curve2.GetPoint(mybin, px2, py2)

		# the other script calculates py2 from px1 so we'll do that as well
		clo_py2 = curve2.Eval(px1)
		ratC.SetPoint(countPoint, px1, clo_py2/py1) #so ratio is curve 2 / curve 1
		#ratC.SetPoint(countPoint, px1, py2/py1) #so ratio is curve 2 / curve 1

		if "Errors" in ratC.ClassName(): ratC.SetPointError(countPoint,0,0)
		countPoint+=1


	ratC.Draw("C")
	myCx.Update()

	myCx.Print(odir+"/bVS"+flav+"__"+tag+".png") #MV2c10

#input("Press Enter to Continue")


# we compare by looping over points of curve and curve2

for bin1 in xrange(0,curve.GetN()): # includes under/overflow bins
    px1=Double(0)
    py1=Double(0)
    curve.GetPoint(bin1,px1,py1)
    #px2=Double(0)
    #py2=Double(0)
    #curve2.GetPoint(bin1,px2,py2)
    clopy2 = curve2.Eval(px1)
    if py1 != clopy2: print("Not the same")
    #print(py1 - clopy2)

