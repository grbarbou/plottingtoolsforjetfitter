# simple macro to add all llr histograms across multiple root ntuples

from glob import glob
import os, sys
from array import *
from ROOT import *#TCanvas,TLegend,gROOT
import math
from math import sqrt
import time

gROOT.LoadMacro("AtlasStyle.C")
gROOT.LoadMacro("AtlasUtils.C")
SetAtlasStyle()

tmpVale=sys.argv

def Helper():
    print " "
    print " Usage: python PrintROC.py <outFolder>  <fileName> or <folderName>"
    print "  ... outFolder MUST containt LC or EM "
    print " "
    sys.exit(1)

if len(sys.argv) <3: Helper()

odir = sys.argv[1]
if ".root" in odir:
    print " ..... outputfolder is a file .... please correct"
    sys.exit(1)

fileList=[]

count = 2
while (count < len(sys.argv)):
    second=sys.argv[count]
    if "," in second:
        fileList=second.split(",")
    else:
        if ".root" in second:
            test=TFile(second)
            if test==None:
                print " ..... input file: "+test+" ... NOT FOUND"
                sys.exit(1)
            fileList.append(second)
        else:
            print "ls -la "+second+"/*.root*"
            fileList=glob(second+"/*.root*")
    count+=1

if len(fileList)==0:
    print(" ..... no files selected .... please check")
    sys.exit(1)
else:
    print(" running over: ", fileList)


gROOT.SetBatch(True)
########################################################################################

taggers=[]

taggers = [
    ["MV2c10", "mv2c10", -1.01, 1.01, 1000, 4],
    #["MV2c20", "mv2c20", -1.01, 1.01, 1000, 7],
    #["IP3D", "ip3d_llr", -12., 30, 1000, 8],
    #["IP2D", "ip2d_llr", -12., 30, 1000, 10],
    #["SV1", "sv1_llr", -4., 13, 1000, 6],
    #["MVb", "mvb", -1.01, 1.01, 1000, 920],
    ["JetFitter", "jf_llr", -15, 10, 1000, 40]
]
effThreshold=0.70

def GetHisto(tag, intree, val):
    tmpH = TH1F(tag[1] + str(val), tag[1] + str(tag[5]), tag[4], tag[2], tag[3]) #defined a weirdly named TH1F
    tmpH.Sumw2() #errors are sumw2
    var="jet_"+tag[1]+">>"+tmpH.GetName() # is a string like jet_mv2c10>>mv2c105
    cut=" jet_LabDr_HadF=="+str(val)+" && abs(jet_eta)<2.5 && jet_pt>20e3  &&  (jet_JVT>0.641 || jet_pt>50e3 || abs(jet_eta)>2.4) && jet_aliveAfterOR==1 "
    intree.Draw( var, cut,"goof") # we process the variable var according to the cut

    #now we do all the adding from intree files to our tmpH
    tmpH.SetBinContent(1,tmpH.GetBinContent(1)+tmpH.GetBinContent(0))
    tmpH.SetBinError(1,sqrt(pow(tmpH.GetBinError(1),2)+pow(tmpH.GetBinError(0),2)))
    tmpH.SetBinContent(0,0.0)
    tmpH.SetBinError(0,0.0)
    tmpH.SetBinContent(tmpH.GetNbinsX(),tmpH.GetBinContent(tmpH.GetNbinsX())+tmpH.GetBinContent(tmpH.GetNbinsX()+1))
    tmpH.SetBinError(tmpH.GetNbinsX(),sqrt(pow(tmpH.GetBinError(tmpH.GetNbinsX()),2)+pow(tmpH.GetBinError(tmpH.GetNbinsX()+1),2)))
    tmpH.SetBinContent(tmpH.GetNbinsX()+1,0.0)
    tmpH.SetBinError(tmpH.GetNbinsX()+1,0.0)
    if val!=4:
        c = TCanvas("_Discr","_Discr",800,600)
        tmpH.GetXaxis().SetTitle(tag[0])
        tmpH.Draw("E")
        c.Update()
        c.Print(odir+"/test.eps")
        time.sleep(0.5)
    return tmpH

def GetInt(tmpH):
    tmpInt=-1
    tmpInt=tmpH.Integral(-10,tmpH.GetNbinsX()+10)
    if tmpH.Integral():
        tmpH.Scale(1./tmpH.Integral(-10,tmpH.GetNbinsX()+10))
    else: print tmpH.Integral(-10,tmpH.GetNbinsX()+10)
    return tmpH, tmpInt

def GetROC(hbkgd,hsig):

    hbkgd, intBkgd = GetInt(hbkgd)
    hsig, intSig = GetInt(hsig)

    if bVSlight:
        found = False
        for bin in xrange(1, hsig.GetNbinsX() + 2):
            partInt = hsig.Integral(bin, hsig.GetNbinsX() + 10)
            if partInt < effThreshold and not found:
                print(" CUT= " + str(hsig.GetBinCenter(bin)) + " has eff: " + str(partInt))
                found = True

    myROC = TGraphErrors()  ##hsig.GetNbinsX()-2 );
    maxRej = 1
    count = -1
    for bin in xrange(2, hsig.GetNbinsX()):
        sigEff = hsig.Integral(bin, hsig.GetNbinsX() + 10)
        bkgdEff = hbkgd.Integral(bin, hsig.GetNbinsX() + 10)
        ##if bVSlight: print str(bkgdEff)+"   "+str(sigEff)+" CUT: "+str(hsig.GetBinCenter(bin))
        if bkgdEff != 0 and sigEff != 0 and sigEff < 0.99:
            ex = sqrt(sigEff * (1 - sigEff) / intSig)
            ey = sqrt(bkgdEff * (1 - bkgdEff) / intBkgd)
            ##if bVSlight: print str(bkgdEff)+"   "+str(sigEff)
            count += 1
            myROC.SetPoint(count, sigEff, 1 / bkgdEff);
            myROC.SetPointError(count, ex, ey / (bkgdEff * bkgdEff))
            if 1 / bkgdEff > maxRej: maxRej = 1 / bkgdEff
    myROC.SetLineWidth(3)
    return myROC, maxRej


#########################################################################################
gSystem.Exec("mkdir -p "+odir)

intree=None
intree=TChain("bTag_AntiKt4EMTopoJets")
print('ok')

for file in fileList:
    intree.Add(file)

print("    ")
print('ok')
print("TOTAL NUMBER OF ENTRIES IS: "+str(intree.GetEntries()))
print("    ")

rocinput = intree

#ofile=TFile(odir+"/histos.root","RECREATE")
ofile=TFile("histos.root","RECREATE") # for grid submission

countT=-1
light=TH1F("b VS light","b VS light",100,0.3,1);
light.SetTitle(";b-jet efficiency;light-jet rejection;")
lightCurve=[]

for tag in taggers:
    hsig = None
    hbkgd = None

    hsig = GetHisto(tag, intree, 5)
    hbkgd=GetHisto(tag, intree,0) # light
    hbkgd2=GetHisto(tag, intree,4) # c

    hsig.Write()
    hbkgd.Write()
    hbkgd2.Write()
    bVSlight = True
    curve,Rej=GetROC(hbkgd, hsig)
    curve.SetLineColor(tag[5])
    lightCurve.append(curve)
    if Rej * 2 > light.GetMaximum(): light.SetMaximum(Rej * 2)


#default nonsense
myC=TCanvas( "bVSl", "bVSl",900,900);
myC.SetLogy()
myC.SetGridy()
myC.SetGridx()
light.SetMinimum(1)
light.SetMaximum(1e5)
light.Draw()
myLumi= "t#bar{t} simulation,"
myLumi+=" 13 TeV, rel19"
myLumi2= "jet p_{T}>25 GeV, |#eta|<2.5"
myText(0.20,0.24,1,myLumi,0.045)
myText(0.20,0.19,1,myLumi2,0.045)
legend4=TLegend(0.70,0.60,0.92,0.92)
legend4.SetTextFont(42)
legend4.SetTextSize(0.04)
legend4.SetFillColor(0)
legend4.SetLineColor(0)
legend4.SetFillStyle(0)
legend4.SetBorderSize(0)
count=-1


# actually draw roc curves onto canvas and save to the histos.root
for curve in lightCurve:
    curve.Draw("C")
    count += 1
    legend4.AddEntry(curve,taggers[count][0],"L")
    curve.SetName(taggers[count][0].replace("+","_")+"---bl")
    curve.Write()
legend4.Draw()
myC.Update()
myC.Print(odir+"/bVSlight.eps")
myC.Print(odir+"/bVSlight.png")
myC.Print(odir+"/bVSlight.C")


ofile.Close()
