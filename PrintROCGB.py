# import argparse
from glob import glob
import numpy as np
import os, sys
from array import *
from ROOT import *  # TCanvas,TLegend,gROOT
import ROOT
import math
import time
import copy

ROOT.gROOT.LoadMacro("AtlasStyle.C")
ROOT.gROOT.LoadMacro("AtlasUtils.C")
SetAtlasStyle()

tmpVale = sys.argv


def Helper():
    print(" ")
    print(" Usage: Follows on from AddHistos.py")
    print("python PrintROCGB.py <outFolder>  <fileNameOfCombinedHistos>")
    print("use hadd for multiple Combined histo files")
    print("  ... outFolder MUST containt LC or EM ")
    sys.exit(1)


if len(sys.argv) != 3: Helper()

odir = sys.argv[1]
if ".root" in odir:
    print(" ..... outputfolder is a file .... please correct")
    sys.exit(1)

fileList=[]
infile = TFile(sys.argv[2])

count = 2
while (count < len(sys.argv)):
    second=sys.argv[count]
    if "," in second:
        fileList=second.split(",")
    else:
        if ".root" in second:
            test=TFile(second)
            if test==None:
                print(" ..... input file: "+test+" ... NOT FOUND")
                sys.exit(1)
            fileList.append(second)
        else:
            print("ls -la "+second+"/*.root*")
            fileList=glob(second+"/*.root*")
    count+=1

if len(fileList)==0:
    print(" ..... no files selected .... please check")
    sys.exit(1)
else:
    print(" running over: ", fileList)


gROOT.SetBatch(True)
########################################################################################

taggers=[]

taggers = [
    ["MV2c10", "mv2c10", -1.01, 1.01, 1000, 4],
    #["MV2c20", "mv2c20", -1.01, 1.01, 1000, 7],
    #["IP3D", "ip3d_llr", -12., 30, 1000, 8],
    #["IP2D", "ip2d_llr", -12., 30, 1000, 10],
    #["SV1", "sv1_llr", -4., 13, 1000, 6],
    #["MVb", "mvb", -1.01, 1.01, 1000, 920],
    ["JetFitter", "jf_llr", -15, 10, 1000, 40]
]
effThreshold=0.70


def GetHistofromFile(tag, infile, val):
    tmpH=infile.Get(tag[1]+str(val))
    tmpH2 = copy.copy(tmpH)
    return tmpH2

def GetInt(tmpH):
    tmpInt=-1
    tmpInt=tmpH.Integral(-10,tmpH.GetNbinsX()+10)
    if tmpH.Integral():
        tmpH.Scale(1./tmpH.Integral(-10,tmpH.GetNbinsX()+10))
    else: print(tmpH.Integral(-10,tmpH.GetNbinsX()+10))
    return tmpH, tmpInt

def GetROC(tag, infile, bVSlight):
    hsig = None
    hbkgd = None
    intSig = -1
    intBkgd = -1
    hsig = GetHistofromFile(tag, infile, 5)
    print(hsig)
    if not bVSlight:
        hbkgd = GetHistofromFile(tag, infile, 4)
    else:
        hbkgd = GetHistofromFile(tag, infile, 0)

    print(bVSlight)
    print("intsig = " + str(intSig))
    hbkgd, intBkgd = GetInt(hbkgd)
    hsig, intSig = GetInt(hsig)   #  for some reason this gives a different answer depending on whether bVSlight is true or not
    print("intsig = " + str(intSig))
    if bVSlight:
        found = False
        for bin in xrange(1, hsig.GetNbinsX() + 2):
            partInt = hsig.Integral(bin, hsig.GetNbinsX() + 10)
            if partInt < effThreshold and not found:
                print(" CUT= " + str(hsig.GetBinCenter(bin)) + " has eff: " + str(partInt))
                found = True

    myROC = TGraphErrors()  ##hsig.GetNbinsX()-2 );
    maxRej = 1
    count = -1
    for bin in xrange(2, hsig.GetNbinsX()):
        sigEff = hsig.Integral(bin, hsig.GetNbinsX() + 10)
        bkgdEff = hbkgd.Integral(bin, hsig.GetNbinsX() + 10)
        ##if bVSlight: print str(bkgdEff)+"   "+str(sigEff)+" CUT: "+str(hsig.GetBinCenter(bin))
        if bkgdEff != 0 and sigEff != 0 and sigEff < 0.99:
            ex = np.sqrt(sigEff * (1 - sigEff) / intSig)
            ey = np.sqrt(bkgdEff * (1 - bkgdEff) / intBkgd)
            ##if bVSlight: print str(bkgdEff)+"   "+str(sigEff)
            count += 1
            myROC.SetPoint(count, sigEff, 1 / bkgdEff)
            myROC.SetPointError(count, ex, ey / (bkgdEff * bkgdEff))
            if 1 / bkgdEff > maxRej: maxRej = 1 / bkgdEff
    myROC.SetLineWidth(3)
    return myROC, maxRej


#########################################################################################
gSystem.Exec("mkdir -p " + odir)

countT = -1
light = TH1F("b VS light", "b VS light", 100, 0.3, 1)
light.SetTitle(";b-jet efficiency;light-jet rejection;")
lightCurve = []

cj = TH1F("b VS c", "b VS c", 100, 0.3, 1)
cj.SetTitle(";b-jet efficiency;c-jet rejection;")
cCurve = []

for tag in taggers:
    countT += 1
    print("<<<<<<<  " + tag[0] + "   >>>>>>>>>>>>>>")
    curve, Rej = GetROC(tag, infile, True)
    curve.SetLineColor(tag[5])
    lightCurve.append(curve)
    if Rej * 2 > light.GetMaximum(): light.SetMaximum(Rej * 2)

    curve2, Rej = GetROC(tag, infile, False)
    curve2.SetLineColor(tag[5])
    cCurve.append(curve2)
    if Rej * 2 > cj.GetMaximum(): cj.SetMaximum(Rej * 2)


ofile = TFile(odir + "/output.root", "RECREATE")

myC = TCanvas("bVSl", "bVSl", 900, 900);
myC.SetLogy()
myC.SetGridy()
myC.SetGridx()
light.SetMinimum(1)
light.SetMaximum(1e5)
light.Draw()
myLumi = "t#bar{t} simulation,"
myLumi += " 13 TeV, rel19"
myLumi2 = "jet p_{T}>25 GeV, |#eta|<2.5"
myText(0.20, 0.24, 1, myLumi, 0.045)
myText(0.20, 0.19, 1, myLumi2, 0.045)
legend4 = TLegend(0.70, 0.60, 0.92, 0.92)
legend4.SetTextFont(42)
legend4.SetTextSize(0.04)
legend4.SetFillColor(0)
legend4.SetLineColor(0)
legend4.SetFillStyle(0)
legend4.SetBorderSize(0)
count = -1
for curve in lightCurve:
    curve.Draw("C")
    count += 1
    legend4.AddEntry(curve, taggers[count][0], "L")
    # ofile.WriteObject(curve,taggers[count][0].replace("+","_")+"---bl")
    curve.SetName(taggers[count][0].replace("+", "_") + "---bl")
    curve.Write()
legend4.Draw()
myC.Update()
myC.Print(odir + "/bVSlight.eps")
myC.Print(odir + "/bVSlight.png")
myC.Print(odir + "/bVSlight.C")

myC2 = TCanvas("cVSl", "cVSl", 900, 900);
myC2.SetLogy()
myC2.SetGridy()
myC2.SetGridx()
cj.SetMinimum(1)
cj.SetMaximum(1e3)
cj.Draw()
count = -1
for curve in cCurve:
    curve.Draw("C")
    count += 1
    ofile.WriteObject(curve, taggers[count][0].replace("+", "_") + "---bc")
legend4.Draw()
myText(0.20, 0.24, 1, myLumi, 0.045)
myText(0.20, 0.19, 1, myLumi2, 0.045)
myC2.Update()
myC2.Print(odir + "/cVSlight.eps")
myC2.Print(odir + "/cVSlight.png")
myC2.Print(odir + "/cVSlight.C")

ofile.Close()

##time.sleep(111111)
